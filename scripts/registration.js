/**
 * sets html min and max value for input with type date
 * check if native form validation or native date validation is not avaible
 * does checks if needed, final validation birthdate more than 18 years ago in every case
 */
$(function(){
	$('#Geburtsdatum').attr('max',extractISODate(getTodayNYearsAgo(18)));
	$('input').change(function(){
		$(this).removeClass('invalid');
		if(!validateInput($(this))){
			$(this).addClass('invalid');
		}
		validateRegistrationForm();
	});
	$('input').focusout(function(){
		$(this).removeClass('invalid');
		if(!validateInput($(this))){
			$(this).addClass('invalid');
		}
		validateRegistrationForm();
	});
	$('input:checkbox').click(function(){
		$(this).parent().removeClass('invalid');
		if(!validateCheckbox($(this))){
			$(this).parent().addClass('invalid');
		}
		validateRegistrationForm();
	});
	$('#Geburtsdatum').change(function(){
		$(this).addClass('invalid');
		if(validateBirthdate($(this))){
			$(this).removeClass('invalid');
		}
		validateRegistrationForm();
	});
	$('#Geburtsdatum').focusout(function(){
		$(this).addClass('invalid');
		if(validateBirthdate($(this))){
			$(this).removeClass('invalid');
		}
		validateRegistrationForm();
	});
});
/**
 *
 */
function validateBirthdate(element){
	if(!hasFormValidation() || !hasNativeDateInput()){
		var elementValue = element.val();
		var resultValue = getNormalizedDateString('#Geburtsdatum');
		if(elementValue === resultValue){
			return false;
		}
		if(convertToDate(resultValue)>getTodayNYearsAgo(18)){
			return false;
		}
	}else{
		return element[0].checkValidity();
	}
	return true;
};
/**
 *
 */
function validateCheckbox(element){
	return hasFormValidation() ? element[0].checkValidity() : element.prop('checked');
};
/**
 *
 */
function validateInput(element){
	if(!hasFormValidation()){
		var currentValue = element.val();
		if(element.prop('required') && currentValue.length == 0){
			return false;
		}
		if(element.prop('pattern')){
			var regex = new RegExp(element.prop('pattern'));
			if (!regex.test(currentValue)) {
				return false;
        	}
		}
	}else{
		return element[0].checkValidity();
	}
	return true;
};
/**
 *	complete registration validation
  */
function validateRegistrationForm(){
	var overAllResult = true;
	$('input').each(function() {
		overAllResult = overAllResult && validateInput($(this));
	});
	$('input:checkbox').each(function(){
		overAllResult = overAllResult && validateCheckbox($(this));
	});
	$('#Geburtsdatum').each(function(){
		overAllResult = overAllResult && validateBirthdate($(this));
	});
	if(!overAllResult){
		$('#submit').attr('disabled','disabled');
	}else{
		$('#submit').removeAttr('disabled');
	}
};
/**
 * returns current date minusYears in the past
 */
function getTodayNYearsAgo(minusYears){
	var calculatedDate = new Date();
	calculatedDate.setFullYear(calculatedDate.getFullYear()-minusYears);
	return calculatedDate;
}
/**
 * returns extracts date of iso timestamp
 */
function extractISODate(dateValue){
	return dateValue.toISOString().split('T')[0];
}
/**
 * converts dd.mm.yyyy string into a date
 */
function convertToDate(dateString){
	var dateSplitted = dateString.split('.');
	return new Date(dateSplitted[2],dateSplitted[1]-1,dateSplitted[0]);
}